This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## You need to use [json-server](https://github.com/typicode/json-server) for backend on the port 3001.
### db.json file in the root folder
```
json-server --watch db.json --port 3001
```


