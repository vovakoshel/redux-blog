import PostsService from "../services/posts-service.js";

const editPost = post => {
  return {
    type: "EDIT_POST",
    post: post
  };
};

const updatePostFromDraft = () => {
  return {
    type: "UPDATE_POST"
  };
};

const fetchPosts = posts => {
  return {
    type: "FETCH_POSTS"
  };
};

const fetchedPosts = posts => {
  return {
    type: "FETCHED_POSTS",
    posts
  };
};

const performPostsFetch = () => {
  return async dispatch => {
    dispatch(fetchPosts());
    const posts = await PostsService.fetchAll();
    dispatch(fetchedPosts(posts.reverse()));
  };
};

const updateDraft = draft => {
  return {
    type: "UPDATE_DRAFT",
    draft
  };
};

const createPost = () => {
  return {
    type: "CREATE_POST"
  };
};

const deletePost = id => {
  return {
    type: "DELETE_POST",
    id
  };
};

export default {
  editPost,
  performPostsFetch,
  updateDraft,
  createPost,
  deletePost,
  updatePostFromDraft
};
