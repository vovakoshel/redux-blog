import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import { Provider } from "react-redux";
import store from "./store.js";

import postsActions from "./actions/posts-actions.js";

import "./index.css";

const onRendered = () => {
  store.dispatch(postsActions.performPostsFetch());
};

const rootComponent = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(rootComponent, document.getElementById("root"), onRendered);
