import React, { Component } from "react";
import ReactQuill from "react-quill";

import store from "../store.js";
import actions from "../actions/posts-actions.js";

import "react-quill/dist/quill.snow.css";

const editor = {
  modules: {
    toolbar: [
      [{ header: "1" }, { header: "2" }, { font: [] }],
      [{ size: [] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" }
      ],
      ["link", "image", "video"],
      ["clean"]
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false
    }
  },
  formats: [
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "video"
  ]
};

class Editor extends Component {
  onUpdate(updated) {
    const updatedPost = {
      title: this.props.article.title || "",
      body: this.props.article.body || "",
      ...updated
    };

    if (this.props.article.id !== undefined) {
      updatedPost.id = this.props.article.id;
    }

    store.dispatch(actions.updateDraft(updatedPost));
  }

  onTitleChange = e => {
    this.onUpdate({ title: e.target.value });
  };

  onBodyChange = body => {
    this.onUpdate({ body });
  };

  onSubmit = mode => {
    if (mode === "new") {
      store.dispatch(actions.createPost());
    } else {
      store.dispatch(actions.updatePostFromDraft());
    }
    this.props.history.push("/posts");
  };

  render() {
    const mode = this.props.article.id === undefined ? "new" : "edit";

    return (
      <div className="bg-editor py-3">
        <h3>Title</h3>
        <input
          type="text"
          className="form-control my-3"
          onInput={this.onTitleChange}
          value={this.props.article.title}
        />

        <h3>Content</h3>
        <ReactQuill
          theme="snow"
          onChange={this.onBodyChange}
          value={this.props.article.body}
          modules={Editor.modules}
          formats={Editor.formats}
        />

        <button
          className="btn btn-outline-success btn-block mt-3"
          onClick={this.onSubmit.bind(this, mode)}
        >
          {mode === "new" ? "Publish" : "Update"}
        </button>
      </div>
    );
  }
}

Editor.modules = editor.modules;
Editor.formats = editor.formats;

export default Editor;
