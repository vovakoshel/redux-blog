import React, { Component } from "react";
import { Link } from "react-router-dom";

class Sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <Link to="/">
          <i className="fas fa-hashtag fa-9x text-center d-inline-block col-12 py-5 logo" />
        </Link>

        <Link to="/new">
          <span className="p-4 py-4 d-inline-block">
            <i className="fas fa-plus mr-4" />
            New post
          </span>
        </Link>

        <Link to="/posts">
          <span className="p-4 py-4 d-inline-block">
            <i className="fas fa-list-ul mr-4" />
            Show all posts
          </span>
        </Link>
      </div>
    );
  }
}

export default Sidebar;
