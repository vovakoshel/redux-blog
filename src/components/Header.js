import React from "react";
import PropTypes from "prop-types";

const Header = props => {
  const { branding } = props;
  return (
    <nav className="navbar navbar-expand-sm bg-danger navbar-dark py-0">
      <div className="container-fluid">
        <a href="/" className="navbar-brand">
          {branding}
        </a>
      </div>
    </nav>
  );
};

// Default props
Header.defaultProps = {
  branding: "My App"
};
// prop types
Header.propTypes = {
  branding: PropTypes.string.isRequired
};

export default Header;
