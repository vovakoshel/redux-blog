import React, { Component } from "react";
import Article from "./Article";

class Articles extends Component {
  render() {
    const { articles } = this.props;

    return (
      <div className="my-4">
        <h2>You've got {articles.length} posts</h2>
        {articles.map(article => {
          return <Article key={article.id} article={article} />;
        })}
      </div>
    );
  }
}

export default Articles;
