import React, { Component } from "react";
import { Link } from "react-router-dom";

import store from "../store.js";
import actions from "../actions/posts-actions.js";

class Article extends Component {
  state = {
    expended: false
  };

  onPostToggle = () => {
    this.setState({
      expended: !this.state.expended
    });
  };

  onPostDelete = () => {
    const { id } = this.props.article;
    store.dispatch(actions.deletePost(id));
  };

  onPostEdit = () => {
    store.dispatch(actions.editPost(this.props.article));
  };

  render() {
    const { title, body, id } = this.props.article;

    return (
      <div className="card card-body mb-3">
        <h4>
          {title}
          <span style={{ cursor: "pointer", float: "right" }}>
            <i className="fas mr-2 fa-sort-down" onClick={this.onPostToggle} />
            <i className="fas mr-2 fa-trash-alt" onClick={this.onPostDelete} />
            <Link
              to={{ pathname: `/edit/${id}` }}
              className="text-body"
              onClick={this.onPostEdit}
            >
              <i className="fas mr-2 fa-pencil-alt" />
            </Link>
          </span>
        </h4>

        {this.state.expended ? (
          <p dangerouslySetInnerHTML={{ __html: body }} />
        ) : null}
      </div>
    );
  }
}

export default Article;
