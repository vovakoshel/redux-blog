import * as redux from "redux";
import thunk from "redux-thunk";

import rootReducer from "./reducers/index.js";

export default redux.createStore(rootReducer, redux.applyMiddleware(thunk));
