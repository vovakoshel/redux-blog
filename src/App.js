import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import store from "./store.js";
import actions from "./actions/posts-actions.js";

import Editor from "./components/Editor";
import Header from "./components/Header";
import Articles from "./components/Articles";
import Sidebar from "./components/Sidebar";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          {/* <Header branding="Article manager" /> */}
          <div id="app">
            <Sidebar />
            <div className="main container">
              <Switch>
                <Route
                  {...this.props}
                  render={() => (
                    <Articles articles={this.props.articles.items} />
                  )}
                  exact
                  path="/posts"
                />

                <Route
                  exact
                  path="/new"
                  render={({ history }) => (
                    <Editor
                      article={this.props.articles.draft}
                      history={history}
                    />
                  )}
                />
                <Route
                  exact
                  path="/edit/:id"
                  render={({ history, match }) => {
                    const id = Number.parseInt(match.params.id);
                    const article = this.props.articles.items.find(
                      i => i.id === id
                    );

                    if (article === undefined) {
                      history.push("/posts");
                      return null;
                    }

                    return (
                      <Editor
                        article={this.props.articles.draft}
                        history={history}
                      />
                    );
                  }}
                />
              </Switch>
            </div>
          </div>
        </React.Fragment>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    articles: state.posts.posts
  };
};

export default connect(mapStateToProps)(App);
