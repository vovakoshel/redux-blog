const rootEndpoint = "http://localhost:3001";

const fetchAll = () => {
  return fetch(`${rootEndpoint}/posts`).then(res => res.json());
};

export default { fetchAll };
