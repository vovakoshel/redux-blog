import initial from "./initial-state.js";

const imtbl = require("imtbl");

const createDraft = () => {
  return {
    title: "",
    body: ""
  };
};

export default (state, action) => {
  switch (action.type) {
    case "EDIT_POST":
      return imtbl.assocIn(state, ["posts", "draft"], action.post);
    case "UPDATE_POST": {
      const draft = imtbl.getIn(state, ["posts", "draft"]);
      const index = imtbl.getIn(state, ["posts", "items"]).findIndex(i => {
        return i.id === draft.id;
      });

      const temp = imtbl.assocIn(state, ["posts", "items", index], draft);
      return imtbl.assocIn(temp, ["posts", "draft"], createDraft());
    }
    case "FETCH_POSTS":
      return imtbl.assocIn(state, ["posts", "fetching"], true);
    case "FETCHED_POSTS":
      const temp = imtbl.assocIn(state, ["posts", "fetching"], false);
      return imtbl.assocIn(temp, ["posts", "items"], action.posts);
    case "UPDATE_DRAFT":
      return imtbl.assocIn(state, ["posts", "draft"], action.draft);
    case "CREATE_POST": {
      const id =
        Math.max(...imtbl.getIn(state, ["posts", "items"]).map(p => p.id)) + 1;

      const newPost = imtbl.getIn(state, ["posts", "draft"]);
      const temp = imtbl.assocIn(state, ["posts", "draft"], createDraft());

      return imtbl.updateIn(temp, ["posts", "items"], items => {
        return [{ ...newPost, id }, ...items];
      });
    }
    case "DELETE_POST": {
      return imtbl.updateIn(state, ["posts", "items"], items => {
        return items.filter(i => i.id !== action.id);
      });
    }
    default:
      return state || initial;
  }
};
